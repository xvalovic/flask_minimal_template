from flask import Flask
from flask import request
from flask import render_template
import psycopg2
import psycopg2.extras


app = Flask(__name__)


connection = None
cursor = None


@app.before_request
def connect_db():
	global connection
	global cursor
	if (connection is None) or (connection.closed==0):  # aktive spojenie vrati 0 ! 
		connection = psycopg2.connect(dbname='user', user='user', host='localhost', password='', port='5432')
		cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)


@app.route('/')
@app.route('/index')
def hello_world():
    return 'Hello, World!'


@app.route('/person/<int:id_person>', methods=['GET', 'POST', 'DELETE'])
def get_person(id_person):
	if request.method == 'GET':
		return "Person id is %d" % id_person
	elif request.method == 'POST':
		return "Updated person id %d" % id_person
	elif request.method == 'DELETE':
		return "Deleted person id %d" % id_person



@app.route('/persons', methods=['GET'])
def get_persons():
	global cursor
	cursor.execute('SELECT * FROM person ORDER BY id_person DESC')
	persons = cursor.fetchall()
	return render_template('persons.html', title='Persons list', persons=persons)


@app.route('/person/new', methods=['GET', 'POST'])
def new_person():
	global connection
	if request.method == 'GET':
		formData = {
			'first_name': '',
			'last_name': '',
			'nickname': '',
			'id_location': None,
			'gender': '',
			'height': '',
			'birth_day': '',
			'city': '',
			'street_name': '',
			'street_number': '',
			'zip': ''
		}
		return render_template('new_person.html', title='New person', formData=formData)

	else:
		formData = request.form
		try:
			cursor.execute('INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender) \
							VALUES (%s, %s, %s, %s, %s, %s ,%s)', (
							formData['nickname'], 
							formData['first_name'], 
							formData['last_name'], 
							None, #formData['id_location'] if formData['id_location'] else None, 
							formData['birth_day'] if formData['birth_day'] else None, 
							formData['height'] if formData['height'] else None,
							formData['gender'] if formData['gender'] else None)
						) 
			connection.commit()
			message = 'Person succesfully inserted'
		except Exception as e:
			message = e

		return render_template('new_person.html', title='New person', formData=formData, message=message)



if __name__ == '__main__':
	app.run(debug=True)